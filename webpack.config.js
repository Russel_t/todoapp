var path = require('path');
var HtmlWebpackPlugin = require( 'html webpack plugin' );
 
module.exports = {
      mode: 'production',
      performance: {
        hints: 'warning'
      },
      output: {
        pathinfo: false
      },
      optimization: {
        namedModules: false,
        namedChunks: false,
        nodeEnv: 'production',
        flagIncludedChunks: true,
        occurrenceOrder: true,
        sideEffects: true,
        usedExports: true,
        concatenateModules: true,
        splitChunks: {
          hidePathInfo: true,
          minSize: 30000,
          maxAsyncRequests: 5,
          maxInitialRequests: 3,
        },
        noEmitOnErrors: true,
        checkWasmTypes: true,
        minimize: true,
      },

    }