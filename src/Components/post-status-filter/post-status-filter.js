import React, {Component} from 'react';


export default class  PostStatusFilter extends Component {
    constructor(props){
        super(props);
    
        this.state = {
             buttons: [
             { name: 'all', lable: 'Все'},
             {name: 'like', lable: 'Понравилось'}
             ]
        }
    }
    render(){
        const buttons = this.state.buttons.map(({name,lable}) => {
            const active = this.props.filter === name;
            const clazz = active ? 'btn-info' : 'btn-outline-secondary';
            return (
                <button 
                    key={name}
                    type='button' 
                    className={`btn ${clazz}`}
                    onClick={()=>this.props.onFilterSelect(name)}>{lable}</button>
            )
        })
    return (
        <div className="btn-group">
           {buttons}
        </div>
    )
}
}
