import React, {Component} from 'react';
 
import './post-list-item.sass';

export default class PostListItem extends Component {
    
    render() {
        const {label, onDelete, onToggleImportant, onToggleLiked, important, like} = this.props;
        let classNames = 'app-list-item d-flex justify-content-between mt-1';

        if (!important){
            classNames += ' important';
        }

        if (like) {
            classNames += ' like';
        }
    
        return (
        <li className={classNames}>
            <span 
            onClick={onToggleLiked}
            className="app-list-item-label p-2">
                {label}
            </span>
            <div className="d-flex jastify-content-end align-items-center">
                <button 
                onClick={onToggleImportant}
                className="btn-star btn-sm">
                    <i className="fa fa-star"></i>
                    
                </button>
                <button
                onClick={onDelete}
                className="btn-trash btn-sm">
                    <i className="fa fa-trash-o"></i>
                </button>
                <i className="fa fa-heart"></i>
            </div>
        </li>
        )
}
}